// Copyright 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef ABSTRACT_DATA_H_
#define ABSTRACT_DATA_H_

#include <vector>
#include <ostream>

using namespace std;

class AbstractData
{
 public:
  virtual ~AbstractData();
  virtual AbstractData* clone() const = 0;

  virtual void print(vector<unsigned int>& prefix, ostream& out) const = 0;
  virtual const bool setTuples(const vector<vector<unsigned int>>::const_iterator dimensionIt, const float noise);
  virtual AbstractData* getCrispRepresentation() const; /* returns null_ptr if not called on a SparseFuzzyTube or a DenseFuzzyTube */
  virtual void sortTubes();
  
  virtual const float noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const = 0;

 protected:
  static unsigned int cardinalityOfLastDimension;
  static float densityThreshold;
};

#endif /*ABSTRACT_DATA_H_*/
