// Copyright 2010,2011,2012,2013,2014,2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "DenseFuzzyTube.h"

DenseFuzzyTube::DenseFuzzyTube(const vector<pair<unsigned int, float>>& sparseTube) : tube(cardinalityOfLastDimension, 1)
{
  for (const pair<unsigned int, float>& entry : sparseTube)
    {
      tube[entry.first] = entry.second;
    }
}

DenseFuzzyTube* DenseFuzzyTube::clone() const
{
  return new DenseFuzzyTube(*this);
}

void DenseFuzzyTube::print(vector<unsigned int>& prefix, ostream& out) const
{
  unsigned int hyperplaneId = 0;
  for (const float noise : tube)
    {
      if (noise != 1)
	{
	  for (const unsigned int id : prefix)
	    {
	      out << id << ' ';
	    }
	  out << hyperplaneId << ' ' << 1 - noise << endl;
	}
      ++hyperplaneId;
    }
}

const bool DenseFuzzyTube::setTuples(const vector<vector<unsigned int>>::const_iterator dimensionIt, const float noise)
{
  for (const unsigned int element : *dimensionIt)
    {
      tube[element] = noise;
    }
  return false;
}

DenseCrispTube* DenseFuzzyTube::getCrispRepresentation() const
{
  return new DenseCrispTube(tube);
}

const float DenseFuzzyTube::noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const
{
  float noise = 0;
  for (const unsigned int id : *dimensionIt)
    {
      noise += tube[id];
    }
  return noise;
}
