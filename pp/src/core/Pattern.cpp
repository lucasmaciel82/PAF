#include "Pattern.h"

vector<vector<string>> Pattern::ids2Labels;

Pattern::Pattern(const vector<vector<unsigned int>> &nSetParam){
	nSet = nSetParam;
	area = 1;
	last_id = 0;
	g = 0;

	sortedNSet.resize(nSet.size());

	int i = 0;
	for (const auto &dim : nSetParam){
		area *= dim.size();
		for (const auto &vlr : dim)
			sortedNSet[i].insert(vlr);
		i++;
	}
}

Pattern::Pattern(const Pattern &other): nSet(other.getNSet()), area(other.getArea()), membershipSum(other.getMembershipSum()), g(other.getG()) {
	sortedNSet.resize(nSet.size());

	int i = 0;
	for (const auto &dim : nSet){
		for (const auto &vlr : dim)
			sortedNSet[i].insert(vlr);
		i++;
	}
}

Pattern::Pattern(Candidate *c){
	nSet = c->getPattern()->getNSet();
	for (const auto& dim : c->getTargetDim()) {
		nSet[dim].push_back(c->getTargetValue());
	}

	sortedNSet.resize(nSet.size());

	area = 1;

	int i = 0;
	for (const auto &dim : nSet){
		area *= dim.size();
		for (const auto &vlr : dim)
			sortedNSet[i].insert(vlr);
		i++;
	}

	setMembershipSum(c->getNewMembershipSum());
}

ostream& operator<<(ostream& out, const Pattern& patternNode){
	vector<vector<string>>::const_iterator ids2LabelsIt = Pattern::ids2Labels.begin();

    for (int i = 0; i < Pattern::ids2Labels.size(); i++){
        if (ids2LabelsIt != Pattern::ids2Labels.begin()){
            out << " ";
        }

        const vector<unsigned int>& dimension = patternNode.getNSet()[i];
        out << (*ids2LabelsIt)[dimension.front()];
        const vector<unsigned int>::const_iterator end = dimension.end();

        for (vector<unsigned int>::const_iterator elementIt = dimension.begin(); ++elementIt != end; ){
            out << "," << (*ids2LabelsIt)[*elementIt];
        }
        ++ids2LabelsIt;
    }

    return out;
}

void Pattern::setNSet(vector<vector<unsigned int>> &nSetParam){
	nSet = nSetParam;
	area = 1;
	for (const auto &dim : nSetParam)
		area *= dim.size();
}

void Pattern::setMembershipSum(double acc){
	membershipSum = acc;
	if (area == 0) g = 0;
	else g = membershipSum * membershipSum / area;
}

void Pattern::setArea(unsigned int areaParam){
	area = areaParam;
}

const vector<set<unsigned int>>& Pattern::getSortedNSet() const{
	return sortedNSet;
}

const vector<vector<unsigned int>>& Pattern::getNSet() const{
	return nSet;
}

double Pattern::getMembershipSum() const{
	return membershipSum;
}

unsigned int Pattern::getArea() const{
	return area;
}

double Pattern::getG() const{
	return g;
}

bool Pattern::isIn(int dim, int vlr) const{
	return sortedNSet[dim].find(vlr) != sortedNSet[dim].end();
}

bool Pattern::operator<(const Pattern &other) const{
	const auto& sns = other.getSortedNSet();
	for (int i = 0; i < sortedNSet.size(); i++){
		if (sortedNSet[i].size() == sns[i].size()){
			for (auto it1 = sortedNSet[i].begin(), it2 = sns[i].begin(); it1 != sortedNSet[i].end(); it1++, it2++){
				if (*it1 != *it2){
					return *it1 < *it2;
				}
			}
		}
		else return sortedNSet[i].size() < sns[i].size();
	}
	return false;
}

Pattern Pattern::operator+(const Pattern &other) const{
	vector<vector<unsigned int>> ans;
	int i = 0;
	for (const auto& dimensionIt : other.getNSet()){
		set<unsigned int> dim;
		for (const auto& value : dimensionIt){
			dim.insert(value);
		}
		for (const auto& value : nSet[i++]){
			dim.insert(value);
		}

		vector<unsigned int> dimension;
		for (const auto& value : dim)
			dimension.push_back(value);
		ans.push_back(dimension);
	}
	return Pattern(ans);
}

void Pattern::getNTuplesFromNSet(const vector<vector<unsigned int>>::const_iterator& dimensionIt, const vector<vector<unsigned int> >::const_iterator& dimensionEnd, vector<unsigned int>& prefix, vector<vector<unsigned int>>& nTuples){
	if (dimensionIt == dimensionEnd){
		nTuples.push_back(prefix);
	}
	else{
		for (const auto& valueIt : *dimensionIt){
			prefix.push_back(valueIt);
			getNTuplesFromNSet(dimensionIt + 1, dimensionEnd, prefix, nTuples);
			prefix.pop_back();
		}
	}
}

vector<vector<unsigned int>> Pattern::toTuples() const{
	vector<vector<unsigned int> > answer;
	vector<unsigned int> prefix;
	prefix.reserve(nSet.size());
	Pattern::getNTuplesFromNSet(nSet.begin(), nSet.end(), prefix, answer);
	return answer;
}

void Pattern::setIds2Labels(vector<vector<string>>& ids2LabelsParam){
    ids2Labels = ids2LabelsParam;
}
