#include "IO.h"
#include "DataTrie.h"
#include "Solver.h"
#include "../../Parameters.h"

using namespace std;

int main(int argc, char* argv[]){

	IO io(argc, argv);

	if (io.fail() != EX_OK){
		return io.fail();
	}

	vector<Pattern*> patterns;

	try{
		PDEBUG("* READING PATTERNS");

		patterns = io.readNSets();
	}
	catch (IncorrectNbOfDimensionsException e){
		cerr << e.what();
		return EX_DATAERR;
	}

	vector<vector<unsigned int>> tmp(io.getSizes());
	Pattern all_sets(tmp);

	try{
		PDEBUG("* READING TUPLES");

		for (const auto& p : patterns){
			all_sets = all_sets + *p;
		}

		io.readTuples(Solver::alldata);
	}
	catch (IncorrectNbOfDimensionsException e){
		cerr << e.what();
		return EX_DATAERR;
	}
	
	for (int i = 0; i < patterns.size(); i++){
		vector<vector<unsigned int>> aux = patterns[i]->toTuples();
		patterns[i]->setMembershipSum(Solver::alldata.sumSet(aux));
	}

	PDEBUG("* RUNNING SOLVER");

	Solver solver(all_sets);
	solver.solveAll(patterns);

	return EX_OK;
}
