#include "Candidate.h"
#include "Pattern.h"
#include "Solver.h"

Candidate::Candidate(Pattern *p, const vector<int>& pdim, int pvlr){
    pattern = p;
    dim = pdim;
    vlr = pvlr;

    vector<vector<unsigned int>> hyperplane = p->getNSet();
    for (const auto& it : dim) {
        hyperplane[it].clear();
        hyperplane[it].push_back(vlr);
    }

    Pattern aux(hyperplane);
    const auto& tuples = aux.toTuples();

    double hyperplaneSum = Solver::alldata.sumSet(tuples);
    membershipSum = p->getMembershipSum() + hyperplaneSum;

    double lambda_te = membershipSum / (p->getArea() + tuples.size());
    double lambda_t = p->getMembershipSum() / p->getArea();
    if (lambda_te < lambda_t)
        diff = p->getArea() * (lambda_te - lambda_t) * (lambda_te - lambda_t);
    else
        diff = 0;
}

Candidate::Candidate(Candidate *c){
    pattern = c->getPattern();
    dim = c->getTargetDim();
    vlr = c->getTargetValue();
    membershipSum = c->getNewMembershipSum();
    diff = c->getRSSDiff();
}

Candidate::~Candidate(){
}

Pattern* Candidate::getPattern() const{
    return pattern;
}

int Candidate::getTargetValue() const{
    return vlr;
}

vector<int> Candidate::getTargetDim() const{
    return dim;
}

double Candidate::getNewMembershipSum() const{
    return membershipSum;
}

double Candidate::getRSSDiff() const{
    return diff;
}
