import os
import errno
import time
import subprocess


def createfile(filename, content):
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    f = open(filename, 'w')
    f.write("%s" % content)
    f.close()


def validfile(filename):
    if os.path.isfile(filename):
        if bool(os.stat(filename).st_size > 0):
            return True
    return False


def printstyle(col1, col2="", col3=""):
        print("%s: %s %s %s" % (time.strftime('%X  %d/%m/%y'), col1.ljust(30), col2.ljust(45), col3))


def sh(line):
    output = None
    devnull = open(os.devnull, 'w')
    try:
        output = list(filter(None, str(subprocess.check_output([line],  stderr=devnull, shell=True), 'utf-8').split('\n')))
    except subprocess.CalledProcessError as ex:
        if ex.returncode == 1:
            output = list(filter(None, str(ex.output, 'utf-8').split('\n')))
        else:
            output = None

    return output


def print_in_file(line, file):
    if not sh("grep \"%s\" %s" % (line, file)):
        sh("echo \"%s\" >> %s" % (line, file))