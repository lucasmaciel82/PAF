#!/bin/bash
dir=$(pwd)
for d in $1/*/ ; do 
	cd "$d"
	echo -e "\n$d" 
	gnuplot quality.plt
	gnuplot time.plt
	gnuplot size.plt
	gnuplot memory.plt
	cd "$dir"
done
