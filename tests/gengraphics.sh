#!/bin/bash
python3 plots.py ./scripts/plots/res all.csv
cd ./scripts/plots/res/
gnuplot ../templates/key1.plt
gnuplot ../templates/key2.plt
./gen.sh
mv *.eps ../../../graphics/
