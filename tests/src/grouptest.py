import configparser
import copy
import json


class GroupTests(dict):

    def __init__(self, config, group_test_name):
        super().__init__()

        self['group-name'] = group_test_name

        # include fields in cascading overlay
        for global_var in config['Global']:
            self[global_var] = json.loads(config['Global'][global_var])
        for local_var in config[group_test_name]:
            self[local_var] = json.loads(config[group_test_name][local_var])

    def run(self, commands):
        v = sorted(list(filter(lambda i: (isinstance(i[1], list) and not i[0] in ['commands']), self.items())),
                   key=lambda x: x[0],
                   reverse=True)
        t = copy.deepcopy(self)

        def loop_var(variables, test):
            var = copy.deepcopy(variables)
            key, values = var.pop(0)
            for value in values:
                test[key] = value
                if len(var) > 0:
                    loop_var(var, test)
                else:
                    for command in test['commands']:
                        commands[command].run(test)

        loop_var(v, t)


class PAFGroupTests(GroupTests):
    @staticmethod
    def create_all_group_tests(tests_config_file):
        tests_config = configparser.ConfigParser()
        tests_config._interpolation = configparser.ExtendedInterpolation()
        tests_config.read(tests_config_file)

        group_test_list = list()
        for group_test_name in filter(lambda x: x.startswith('GroupTest'), tests_config):
            group_test_list.append(PAFGroupTests(tests_config, group_test_name))

        return group_test_list

    def __init__(self, config, group_test_name):
        super().__init__(config, group_test_name)

    def gen_hidden_pattern_sizes_string(self):
        if 'hidden-pattern-sizes' not in self:
            return

        string = ''
        for p in range(0, self['pattern-quantity']):
            for d in range(0, self['dimension-quantity']):
                string += '%s ' % self['hidden-pattern-sizes']
            string += '\n'
        self['hidden-pattern-sizes-string'] = string

    def gen_dimension_sizes_string(self):
        if 'dimension-sizes' not in self:
            return

        string = ''
        for d in range(0, self['dimension-quantity']):
            string += '%s ' % self['dimension-sizes']
        self['dimension-sizes-string'] = string

    def gen_size_constraint_string(self):
        if 'size-constraint' not in self:
            return

        string = ''
        for d in range(0, self['dimension-quantity']):
            string += '%s ' % self['size-constraint']
        self['size-constraint-string'] = string

    def gen_epsilon_string(self):
        if 'epsilon' not in self:
            return

        string = ''
        for d in range(0, self['dimension-quantity']):
            string += '%s ' % self['epsilon']
        self['epsilon-string'] = string

    def gen_test_number_string(self):
        if 'test' not in self:
            return
        self['test-number-string'] = self['test-number-mask'] % self['test']

    def gen_correct_obs_string(self):
        if 'correct-obs' not in self:
            return
        self['correct-obs-string'] = self['correct-obs-mask'] % self['correct-obs']

    def gen_epsilon(self):
        if ('size-constraint' not in self) or \
                ('epsilon-relative' not in self) or \
                ('dimension-quantity' not in self):
            return

        value = self['size-constraint'] ** (self['dimension-quantity']-1) * self['epsilon-relative']
        self['epsilon'] = "%.2f" % value

    def update_strings(self):
        self.gen_hidden_pattern_sizes_string()
        self.gen_dimension_sizes_string()
        self.gen_size_constraint_string()
        self.gen_epsilon()
        self.gen_epsilon_string()
        self.gen_test_number_string()
        self.gen_correct_obs_string()
