#!/usr/bin/python3
import sys

from src.command import Command
from src.grouptest import PAFGroupTests

commands_config_file = 'commands.ini'
grouptests_config_file = 'grouptests.ini'

if len(sys.argv) == 3:
    commands_config_file = sys.argv[1]
    grouptests_config_file = sys.argv[2]

all_commands = Command.create_all_commands(commands_config_file)
all_group_tests = PAFGroupTests.create_all_group_tests(grouptests_config_file)

for group_test in all_group_tests:
    group_test.run(all_commands)

